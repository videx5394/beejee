<!doctype html>
<head>
    <meta charset="utf-8">

    <title>BeeJee</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

    <style>
        body {
            padding-top: 5rem;
        }
        .starter-template {
            padding: 3rem 1.5rem;
            text-align: center;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>

            <?php
            if(isset($_SESSION) && isset($_SESSION['loggedin']) && isset($_SESSION['loggedin']) == true)
            {
                echo ' <li class="nav-item active">
                            <a class="nav-link" href="/logout">Logout </a>
                        </li>';
            }
            else {
                echo ' <li class="nav-item active">
                            <a class="nav-link" href="/login">Login </a>
                        </li>';
            }
            ?>
        </ul>
    </div>
</nav>

<main role="main" class="container">

    <div class="starter-template">

        <?php
        echo $content_for_layout;

            if (isset($response['status']) && isset($response['message'])) {
                if ($response['status'] == \App\Utils\Validation::SUCCESS_STATUS) {
                    echo '<div class="alert alert-success mt-5"  role="alert">
                            ' . $response['message'] . '
                       </div>';
                } elseif ($response['status'] == \App\Utils\Validation::ERROR_STATUS) {
                    echo '<div class="alert alert-danger mt-5"  role="alert">
                            ' . $response['message'] . '
                       </div>';
                }
            }
        ?>

    </div>


</main>
</body>
</html>
