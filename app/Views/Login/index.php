<h1>Log In</h1>
<form method='post' action='#'>
    <div class="form-group">
        <label for="title">Username</label>
        <input type="text" class="form-control" value="<?php if(isset($username)) echo $username ?>" id="username" placeholder="Enter username" name="username">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" value="<?php if(isset($password)) echo $password ?>" id="password" placeholder="Enter password" name="password">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
