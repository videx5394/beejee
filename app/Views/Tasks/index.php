<?php
if(!isset($_SESSION))
{
    session_start();
}
?>
<style>
    #tasks_wrapper {
        width: 100%;
    }
</style>
<h1>Tasks</h1>
<a href="/tasks/create/" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new task</a>
<div class="row col-md-12 centered">

    <table id="tasks" style="width:100%" class="table table-striped">
        <thead>
        <tr>
            <th>User</th>
            <th>Email</th>
            <th>Description</th>
            <th>Status</th>
            <th>Redacted by admin</th>
            <?php
                if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true)
                {
                    echo '<th class="text-center">Action</th>';
                }
            ?>
        </tr>
        </thead>
    </table>
</div>
<script>
    function editTask(button)
    {
        var tr_id = button.closest('tr').attr('tr_id');
        location.href = '/tasks/edit?id='+tr_id;
    }
    function tableDB()
    {
        var serchData = {};

        table = $('#tasks').DataTable({
            "lengthMenu": [ 3 ],
            "searching": false,
            "paging": true,
            "autoWidth": false,
            'createdRow': function( row, data, dataIndex )
            {
                $(row).attr('tr_id', data.id);
            },
            "columnDefs": [
                {
                    "targets": [4,5],
                    "orderable": false,
                },
                <?php
                if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true)
                {
                    echo '{
                        "targets": 5,
                        "data": null,
                        "defaultContent": "<button class=\'btn btn-primary\' onclick=\'editTask($(this))\'>Edit Task</button>"
                    }';
                }
                ?>

            ],
            'rowCallback': function(row, data, index)
            {

            },
            "ordering": true,
            "order": [[0, "asc"]],
            "orderClasses": false,
            "processing": false,
            "serverSide": true,
            "ajax": {
                url: '/tasks/getTableData',
                data: serchData,
                error: function (e) {
                    console.log(e)
                }
            },
        });

    }

    tableDB();
</script>