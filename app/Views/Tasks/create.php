<h1>Create task</h1>
<form method='post' action='#'>
    <div class="form-group">
        <label for="title">Email</label>
        <input type="email" class="form-control" value="<?php if(isset($email)) echo $email ?>" id="email" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" value="<?php if(isset($description)) echo $description ?>" id="description" placeholder="Enter description" name="description">
    </div>
    <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" value="<?php if(isset($username)) echo $username ?>" id="username" placeholder="Enter username" name="username">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>