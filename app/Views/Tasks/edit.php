<h1>Create task</h1>
<form method='post' action='#'>
    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" value="<?= $description ?>" id="description" placeholder="Enter description" name="description">
    </div>
    <div class="form-group">
        <label for="username">Status</label>

        <select class="form-control"  id="status" name="status">
            <option <?php if($status == 0) echo 'selected'; else echo ''; ?> value="0" >Not Completed</option>
            <option <?php if($status == 1) echo 'selected'; else echo ''; ?> value="1" >Completed</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>


