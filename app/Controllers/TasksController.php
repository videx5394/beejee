<?php

use App\Core\Controller;
use App\Models\Task;
use App\Utils\Validation;

class TasksController extends Controller
{
    public function index()
    {
        $this->render("index");
    }

    public function getTableData($request)
    {

        $start = isset($_GET['start']) ? (int)$_GET['start'] : 0;
        $orderColumnIndex = isset($_GET['order']) && isset($_GET['order'][0]) && isset($_GET['order'][0]['column']) ? (int)$_GET['order'][0]['column'] : 0;
        $orderBy = isset($_GET['order']) && isset($_GET['order'][0]) && isset($_GET['order'][0]['dir']) && in_array($_GET['order'][0]['dir'], ['asc', 'desc']) ? $_GET['order'][0]['dir'] : 'asc';
        $length = isset($_GET['length']) ? (int)$_GET['length'] : 3;

        $task = new Task();
        $sortCredentials = $task->adjustSortCredentials($start, $orderColumnIndex, $length);
        $tasksData = $task->getTasksData($sortCredentials['start'], $sortCredentials['orderColumn'], $orderBy, $sortCredentials['length']);

        $tasks = $tasksData['tasks'];
        $recordsTotal = (int)$tasksData['count'];
        $data = $task->getDataForTasksDataTable($tasks);

        $payload['data'] = $data;
        $payload['recordsFiltered'] = $recordsTotal;
        $payload['recordsTotal'] = $recordsTotal;

        print json_encode($payload);
    }

    public function create($request)
    {
        $payload = [];

        if ($request->requestMethod == 'POST') {
            $validation = new Validation();
            $validation->setMessage('task successfully added!');
            $validation->setStatus(Validation::SUCCESS_STATUS);

            if (isset($_POST['username']) && is_string($_POST['username'])) {
                $validation->validateUserName($_POST['username']);
            } else {
                $validation->setMessage('invalid username');
                $validation->setStatus(Validation::ERROR_STATUS);
            }

            if (isset($_POST['description']) && is_string($_POST['description'])) {
                $validation->validateDescription($_POST['description']);
            } else {
                $validation->setMessage('invalid description');
                $validation->setStatus(Validation::ERROR_STATUS);
            }

            if (isset($_POST['email']) && is_string($_POST['email'])) {
                $validation->validateEmail($_POST['email']);
            } else {
                $validation->setMessage('invalid email');
                $validation->setStatus(Validation::ERROR_STATUS);
            }

            //Save task
            if ($validation->getStatus() == Validation::SUCCESS_STATUS) {
                $task = new Task();
                if (!$task->create($_POST['username'], $_POST['email'], $_POST['description'])) {
                    $validation->setStatus(Validation::ERROR_STATUS);
                    $validation->setMessage('internal error');
                }
            } else {
                $payload['username'] = $_POST['username'];
                $payload['email'] = $_POST['email'];
                $payload['description'] = $_POST['description'];
            }
            $payload['response'] = [
                'message' => $validation->getMessage(),
                'status' => $validation->getStatus()
            ];
        }

        $this->set($payload);
        $this->render("create");
    }

    public function edit($request)
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        // tasks can be modified only by logged admins
        if (!(isset($_SESSION['loggedin']) && isset($_SESSION['loggedin']) == true)) {
            header('location: /login');
            exit;
        }

        $task = new Task();
        $taskId = isset($_GET['id']) ? (int)$_GET['id'] : 0;
        $fetchedTask = $task->getTask($taskId);
        if (!$fetchedTask) {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
            exit;
        }
        $payload['description'] = $fetchedTask['description'];
        $payload['status'] = $fetchedTask['status'];

        if ($request->requestMethod == 'POST') {
            $validation = new Validation();
            $validation->setMessage('task successfully updated!');
            $validation->setStatus(Validation::SUCCESS_STATUS);

            if (isset($_POST['description']) && is_string($_POST['description'])) {
                $validation->validateDescription($_POST['description']);
            } else {
                $validation->setMessage('invalid description');
                $validation->setStatus(Validation::ERROR_STATUS);
            }

            if (isset($_POST['status'])) {
                $validation->validateTaskStatus($_POST['status']);
            } else {
                $validation->setMessage('invalid status');
                $validation->setStatus(Validation::ERROR_STATUS);
            }

            //Update task
            if ($validation->getStatus() == Validation::SUCCESS_STATUS) {
                $task = new Task();
                if (!$task->edit($taskId, $_POST['description'], $_POST['status'])) {
                    $validation->setStatus(Validation::ERROR_STATUS);
                    $validation->setMessage('internal error');
                }
            }

            $payload['status'] = $_POST['status'];
            $payload['description'] = $_POST['description'];

            $payload['response'] = [
                'message' => $validation->getMessage(),
                'status' => $validation->getStatus()
            ];
        }
        $this->set($payload);
        $this->render("edit");
    }


}