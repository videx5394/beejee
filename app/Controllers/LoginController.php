<?php

use App\Core\Controller;
use App\Utils\Validation;
use App\Models\Admin;

class LoginController extends Controller
{
    public function index($request)
    {
        if(!isset($_SESSION))
        {
            session_start();
        }
        // redirect if already logged in
        if (isset($_SESSION['loggedin']) && isset($_SESSION['loggedin']) == true) {
            header('location: /');
            exit;
        }

        $payload = [];

        if ($request->requestMethod == 'POST') {
            // Validation
            $validation = new Validation();
            $validation->setMessage('Logged in successfully!');
            $validation->setStatus(Validation::SUCCESS_STATUS);

            if (isset($_POST['username']) && is_string($_POST['username'])) {
                $validation->validateUserName($_POST['username']);
            } else {
                $validation->setMessage('invalid username');
                $validation->setStatus(Validation::ERROR_STATUS);
            }

            if (isset($_POST['password']) && is_string($_POST['password'])) {
                $validation->validatePassword($_POST['password']);
            } else {
                $validation->setMessage('invalid password');
                $validation->setStatus(Validation::ERROR_STATUS);
            }

            if ($validation->getStatus() == Validation::SUCCESS_STATUS) {
                $admin = new Admin();

                $fetchedAdmin = $admin->getAdmin($_POST['username'], $_POST['password']);

                if ($fetchedAdmin === false) {
                    $validation->setStatus(Validation::ERROR_STATUS);
                    $validation->setMessage('invalid credentials');
                } else {
                    return $admin->login($fetchedAdmin['id'], $fetchedAdmin['username']);
                }
            } else {
                $payload['username'] = $_POST['username'];
                $payload['password'] = $_POST['password'];
            }
            $payload['response'] = [
                'message' => $validation->getMessage(),
                'status' => $validation->getStatus()
            ];
        }
        $this->set($payload);
        $this->render("index");
    }

    public function logout()
    {
        // Destroy the session.
        if(!isset($_SESSION))
        {
            session_start();
        }
        $_SESSION = array();
        session_destroy();
        header("location: /");
        exit;
    }
}