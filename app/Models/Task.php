<?php

namespace App\Models;

use App\Core\Model;
use App\Config\Database;
use PDO;

class Task extends Model
{

    const STATUS_COMPLETED = 'Completed';
    const STATUS_NOT_COMPLETED = 'Not Completed';
    const REDACTED = 'Redacted';
    const NOT_REDACTED = 'Not Redacted';

    private static $tasksColumns = [
        0 => 'username',
        1 => 'email',
        2 => 'description',
        3 => 'status',
    ];

    public function adjustSortCredentials($start,$orderColumnIndex,$length)
    {
        $colums = self::$tasksColumns;

        $start = ($start <= 0) ? 0 : $start;
        $orderColumnIndex = array_key_exists($orderColumnIndex, $colums) ? $orderColumnIndex : 0;
        $orderColumn = in_array($orderColumnIndex, array_keys($colums)) ? $colums[$orderColumnIndex] : $colums[0];
        $length = ($length < 3) ? 3 : $length;

        return [
            'start'         => $start,
            'orderColumn'  => $orderColumn,
            'length'        => $length
        ];
    }


    public function getTasksData($start,$orderColumn,$orderBy,$length) :array
    {
        $countSql = "SELECT COUNT(0) FROM tasks";
        $request = Database::getBdd()->prepare($countSql);
        $request->execute();
        $count = $request->fetchColumn();

        $dataSql = "SELECT * FROM tasks ORDER BY $orderColumn $orderBy LIMIT :rowLength OFFSET :start";
        $request = Database::getBdd()->prepare($dataSql);

        $request->bindParam(':rowLength', $length, PDO::PARAM_INT);
        $request->bindParam(':start', $start, PDO::PARAM_INT);
        $request->execute();
        $tasks = $request->fetchAll();

        return [
            'count'=>$count,
            'tasks'=>$tasks,
        ];
    }

    public function getDataForTasksDataTable($tasks) :array
    {
        $data = [];
        foreach ($tasks as $task)
        {
            $data[] = [
                $task['username'],
                $task['email'],
                $task['description'],
                ($task['status']> 0 ? self::STATUS_COMPLETED : self::STATUS_NOT_COMPLETED ),
                ($task['redacted']> 0 ? self::REDACTED : self::NOT_REDACTED ),
                '',
                'id' => $task['id'],
            ];
        }
        return $data;
    }


    public function create($username,$email,$description)
    {
        $sql = "INSERT INTO tasks (username, email, description) VALUES (:username,:email, :description)";
        $request = Database::getBdd()->prepare($sql);
        return $request->execute([
            'username' => $username,
            'email' => $email,
            'description' => $description
        ]);
    }

    public function getTask($id)
    {
        $sql = "SELECT * FROM tasks WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }

    public function showAllTasks()
    {
        $sql = "SELECT * FROM tasks";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function edit($id, $description,$status)
    {
        $sql = "UPDATE tasks SET description = :description, status = :status , redacted = 1 WHERE id = :id";

        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            'id' => $id,
            'status' => $status,
            'description' => $description,
        ]);
    }
}