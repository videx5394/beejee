<?php



namespace App\Models;

use App\Core\Model;
use App\Config\Database;
use PDO;

class Admin extends Model
{
    public function getAdmin($username,$password)
    {
        $query = "SELECT * FROM admins WHERE username=:username AND password=:password";
        $request = Database::getBdd()->prepare($query);

        $request->bindParam(':username', $username, PDO::PARAM_STR);
        $request->bindParam(':password', $password, PDO::PARAM_STR);
        $request->execute();
        return $request->fetch();
    }

    public function login($id,$username)
    {
        if(!isset($_SESSION))
        {
            session_start();
        }
        // Store data in session variables
        $_SESSION["loggedin"] = true;
        $_SESSION["id"] = $id;
        $_SESSION["username"] = $username;
        // Redirect user to home page
        header("location: /");
    }
}