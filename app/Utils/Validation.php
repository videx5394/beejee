<?php

namespace App\Utils;


class Validation
{
    const SUCCESS_STATUS = 'ok';
    const ERROR_STATUS = 'error';

    private $status;
    private $message;

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        return $this->status = $status;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        return $this->message = $message;
    }

    public function validateUserName($username)
    {
        if (!(strlen($username) > 0 && strlen($username) < 255)) {
            $this->setMessage('username length should be more than 0 and less than 255');
            $this->setStatus(self::ERROR_STATUS);
        }
    }

    public function validateTaskStatus($taskStatus)
    {
        if (!in_array($taskStatus,[0,1])) {
            $this->setMessage('invalid status');
            $this->setStatus(self::ERROR_STATUS);
        }
    }

    public function validatePassword($password)
    {
        if (!(strlen($password) > 0 && strlen($password) < 255)) {
            $this->setMessage('password length should be more than 0 and less than 255');
            $this->setStatus(self::ERROR_STATUS);
        }
    }

    public function validateDescription($description)
    {
        if (!(strlen($description) > 0 && strlen($description) < 255)) {
            $this->setMessage('description length should be more than 0');
            $this->setStatus(self::ERROR_STATUS);
        }
    }

    public function validateEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->setMessage('Invalid email format');
            $this->setStatus(self::ERROR_STATUS);
        }
    }
}