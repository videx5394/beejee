<?php
define('WEBROOT', str_replace("Webroot/index.php", "", $_SERVER["SCRIPT_NAME"]));
define('ROOT', str_replace("Webroot/index.php", "", $_SERVER["SCRIPT_FILENAME"]));
require_once '../vendor/autoload.php';

require_once ROOT . 'Request.php';
require_once ROOT . 'Router.php';
$router = new Router(new Request);

//routing

$router->get('/', 'TasksController@index');
$router->get('/tasks/getTableData', 'TasksController@getTableData');
$router->get('/tasks/create', 'TasksController@create');
$router->post('/tasks/create', 'TasksController@create');
$router->get('/tasks/edit', 'TasksController@edit');
$router->post('/tasks/edit', 'TasksController@edit');
$router->get('/tasks/getTableData', 'TasksController@getTableData');
$router->get('/login', 'LoginController@index');
$router->get('/logout', 'LoginController@logout');
$router->post('/login', 'LoginController@index');
