<?php

class Router
{
    private $request;
    private $supportedHttpMethods = array(
        "GET",
        "POST"
    );

    function __construct(IRequest $request)
    {
        $this->request = $request;
    }

    function __call($name, $args)
    {
        list($route, $controllerAction) = $args;

        if (!in_array(strtoupper($name), $this->supportedHttpMethods)) {
            $this->invalidMethodHandler();
        }

        $this->{strtolower($name)}[$this->formatRoute($route)] = $controllerAction;
    }

    /**
     * Removes trailing forward slashes from the right of the route.
     * @param route (string)
     */
    private function formatRoute($route)
    {
        $result = rtrim(strtok($route,'?'), '/');
        if ($result === '') {
            return '/';
        }
        return $result;
    }

    private function invalidMethodHandler()
    {
        require(ROOT . "app/Views/405.php");
        return;
    }

    private function defaultRequestHandler()
    {
        require(ROOT . "app/Views/404.php");
        return;
    }

    /**
     * Resolves a route
     */
    function resolve()
    {
        $controllerActionsDictionary = $this->{strtolower($this->request->requestMethod)};
        $formattedRoute = $this->formatRoute($this->request->requestUri);

        if (!isset($controllerActionsDictionary[$formattedRoute]) || is_null($controllerActionsDictionary[$formattedRoute])) {
            return $this->defaultRequestHandler();
        }
        $controllerAction = $controllerActionsDictionary[$formattedRoute];
        $this->dispatch($controllerAction);
    }


    public function dispatch($controllerAction)
    {
        $explodedControllerActionString = explode('@', $controllerAction);
        $controllerName = $explodedControllerActionString[0];
        $actionName = $explodedControllerActionString[1];
        $controller = $this->loadController($controllerName);
        if (method_exists($controller, $actionName)) {
            call_user_func_array([$controller, $actionName], [$this->request]);
        } else {
            $this->defaultRequestHandler();
        }
    }

    public function loadController($controllerName)
    {
        $file = ROOT . 'app/Controllers/' . $controllerName . '.php';
        if (file_exists($file)) {
            require_once($file);
            $controller = new $controllerName();
            return $controller;
        }
        return null;
    }

    function __destruct()
    {
        $this->resolve();
    }
}